"""
Resolution of fifth day
"""

from typing import List

def __dico(lower, upper, value, range_look):
    if not value:
        return range_look[0]

    size = len(range_look)
    if value[0] == lower:
        update_range = range_look[:size//2]
    else:
        update_range = range_look[size//2:]
    return __dico(lower, upper, value[1:], update_range)

def __get_id_seat(code: str):
    row_c, column_c = code[:7], code[7:]
    row = __dico("F", "B", row_c, list(range(0, 128)))
    column = __dico("L", "R", column_c, list(range(0, 8)))
    return row * 8 + column

def star_1(values: List[str]):
    """
    Retrieve highest seat ID

    :param values: List of boarding pass
    :return: highest seat ID
    """

    return max(list(map(__get_id_seat, values)))

def star_2(values: List[str]):
    """
    Find free seat id on aircraft

    :param values: List of boarding pass
    :return: Free highest ID
    """
    ids = list(map(__get_id_seat, values))
    ids.sort()

    last = -1
    for seat_id in ids:
        if last + 2 == seat_id and 160 < seat_id < 800:
            return seat_id - 1
        last = seat_id
    return None

def solve(filename: str):
    """
    Get data file, and resolve star problems

    :param filename: Input filename
    :return: Solution of first and second star
    """
    #Read data
    with open(filename) as stream:
        datas = list(map(lambda x: x[:-1], stream.readlines()))

    return star_1(datas), star_2(datas)
