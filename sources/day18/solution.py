"""
Resolution of eighteenth day
"""

from typing import List

class NewInteger:
    """
    Class define new integer interpretation
    """
    def __init__(self, number):
        self.number = number
    def __add__(self, number):
        return NewInteger(self.number + number.number)
    def __mul__(self, number):
        return NewInteger(self.number + number.number)
    def __sub__(self, number):
        return NewInteger(self.number * number.number)
    def __str__(self):
        return str(self.number)
#   __repr__=__str__

def eval_expression(equation):
    """
    Evaluate expression

    :param equation: Expression
    :return: Interpretation result
    """
    new_equation = ""
    for element in equation.split():
        if element == "*":
            new_equation += "-"
        elif "(" in element:
            nb_p = element.count("(")
            new_equation += "(" * nb_p
            new_equation += "NewInteger({0})".format(element[nb_p:])
        elif ")" in element:
            nb_p = element.count(")")
            new_equation += "NewInteger({0})".format(element[:-nb_p])
            new_equation += ")" * nb_p
        elif element == "+":
            new_equation += "+"
        else:
            new_equation += "NewInteger({0})".format(element)
    return new_equation

def star_1(values: List[str]) -> int:
    """
    Evaluate expression

    :param equation: Expression
    :return: Interpretation result
    """
    result = 0

    for equation in values:
        new_expression = eval_expression(equation)
        result += eval(new_expression).number

    return result

def star_2(values: List[str]):
    """
    Evaluate expression

    :param equation: Expression
    :return: Interpretation result
    """
    result = 0

    for equation in values:
        new_expression = eval_expression(equation)
        new_expression = new_expression.replace("+", "*")
        result += eval(new_expression).number

    return result

def solve(filename: str):
    """
    Get data file, and resolve star problems

    :param filename: Input filename
    :return: Solution of first and second star
    """

    #Read data
    with open(filename) as stream:
        datas = stream.read().splitlines()

    return star_1(datas), star_2(datas)
