"""
Resolution of nineth day
"""

from typing import List

def star_1(values: List[str], padding: int = 5) -> int:
    """
    Retrieve first number that does not have good property

    :param values: List of numbers
    :return: Wrong number
    """
    datas = list(map(int, values))
    size = len(datas)

    #Start
    possibilities = [[datas[index] + datas[index2] \
        for index2 in range(index +1, padding)]\
        for index in range(0, padding - 1)]

    for index in range(padding, size):
        value_check = datas[index]
        find = False
        for possibility in possibilities:
            if value_check in possibility:
                find = True
                break
        if not find:
            return value_check
        #Update possibilities
        possibilities.pop(0)
        for pos, index2 in enumerate(range(index + 1 - padding, index - 1)):
            possibilities[pos].append(datas[index2] + value_check)
        possibilities.append([datas[index - 1] + value_check])

    return None

def star_2(values: List[str], search: int = 127):
    """
    Retrieve the encryption weakness

    :param values: List of numbers
    :return: Number of weakness
    """
    datas = list(map(int, values))
    size_datas = len(datas)
    start = 0
    end = 1
    number = datas[0]

    while search != number and end < size_datas - 1:
        if number < search:
            number += datas[end]
            end += 1
        else:
            number -= datas[start]
            start += 1

    if search == number:
        return max(datas[start:end]) + min(datas[start:end])

    return None

def solve(filename: str):
    """
    Get data file, and resolve star problems

    :param filename: Input filename
    :return: Solution of first and second star
    """
    #Read data
    with open(filename) as stream:
        datas = stream.readlines()

    return star_1(datas, 25), star_2(datas, 466456641)
