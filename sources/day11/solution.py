"""
Resolution of eleventh day
"""

from typing import List

import numpy as np
from scipy.ndimage import convolve

def star_1(values: List[str]) -> int:
    """
    Simulate sit evolution

    :param values: Sit gird
    :return: Number of free sit
    """
    converter = str.maketrans('.L#', '012')
    grid = np.array([[int(x) for x in list(value.translate(converter))] for value in values])

    chunck = np.array([[1,1,1],[1,0,1],[1,1,1]])
    check = True
    while check:
        grid_chunck = convolve(np.where(grid == 2, 1, 0), chunck, mode='constant')
        last_grid = np.copy(grid)
        grid[(grid == 1) & (grid_chunck == 0)] = 2
        grid[(grid == 2) & (grid_chunck >= 4)] = 1
        check = not (grid == last_grid).all()

    return np.count_nonzero(grid == 2)

def __closest_seat_coord(coord, offset, grid):
    curr_loc = (coord[0] + offset[0], coord[1] + offset[1])
    while 0 <= curr_loc[0] < len(grid) and 0 <= curr_loc[1] < len(grid[curr_loc[0]]) \
        and grid[curr_loc] == 0:
        curr_loc = (curr_loc[0] + offset[0], curr_loc[1] + offset[1])
    return curr_loc

def star_2(values: List[str]):
    """
    Next simulation of sit evolution

    :param values: Sit gird
    :return: Number of free sit
    """
    converter = str.maketrans('.L#', '012')
    grid = np.array([[int(x) for x in list(value.translate(converter))] for value in values])

    vectors = [(-1,-1),(-1,0),(-1,1),(0,-1),(0,1),(1,-1),(1,0),(1,1)]
    neighbours = np.array([[[__closest_seat_coord((x, y), d, grid) for d in vectors]
                            for y, c in enumerate(r)] for x, r in enumerate(grid)])
    padded_seats = np.zeros((grid.shape[0] + 2, grid.shape[1] + 2))
    while True:
        prev_seats = np.copy(grid)
        padded_seats[1:-1, 1:-1] = grid
        neighbour_vals = np.take(padded_seats,
                                    np.ravel_multi_index(np.rollaxis(neighbours + 1, -1),
                                        padded_seats.shape))
        res = np.sum(neighbour_vals == 2, axis=2)
        grid[(grid == 1) & (res == 0)] = 2
        grid[(grid == 2) & (res >= 5)] = 1
        if (prev_seats == grid).all():
            break

    return np.count_nonzero(grid == 2)

def solve(filename: str):
    """
    Get data file, and resolve star problems

    :param filename: Input filename
    :return: Solution of first and second star
    """
    #Read data
    with open(filename) as stream:
        datas = stream.read().splitlines()

    return star_1(datas), star_2(datas)
