"""
Resolution of twelveth day
"""

from math import cos, sin
from typing import List

from aenum import MultiValueEnum
import numpy as np

class Direction(MultiValueEnum):
    """
    Enum define direction of ship
    """
    N = lambda position, value: position + [value, 0], "N"
    E = lambda position, value: position + [0, value], "E"
    S = lambda position, value: position + [-value, 0], "S"
    W = lambda position, value: position + [0, -value], "W"

round_a =  np.vectorize(round)
left_rotation = lambda teta: round_a([[cos(teta), -sin(teta)], [sin(teta), cos(teta)]])

class Ship:
    """
    Class define Ship
    """
    direction = np.array([Direction.N, Direction.E, Direction.S, Direction.W])
    facing: int = 1
    position = np.array([0, 0])
    waypoint = np.array([1, 10])

    def move(self, instruction: str):
        """
        Simulate relative ship move

        :param: Ship instructions
        """
        cumputing = instruction[0]
        number = int(instruction[1:])
        if cumputing in Direction._value2member_map_:
            self.position = Direction(cumputing).value(self.position, number)
        elif cumputing == "F":
            self.position = self.direction[self.facing].value(self.position, number)
        elif cumputing == "L":
            self.facing = (self.facing - number // 90) % 4
        else:
            self.facing = (self.facing + number // 90) % 4

    def vector_move(self, instruction: str):
        """
        Simulate vector ship move

        :param: Ship instructions
        """
        cumputing = instruction[0]
        number = int(instruction[1:])

        if cumputing in Direction._value2member_map_:
            self.waypoint = Direction(cumputing).value(self.waypoint, number)
        elif cumputing == "F":
            self.position = self.position + self.waypoint * number
        else:
            number = number if cumputing != "L" else -number
            number = np.deg2rad(number)
            self.waypoint = np.dot(left_rotation(number), self.waypoint)

def star_1(values: List[str]) -> int:
    """
    Simulate relative ship move

    :param values: Ship instructions
    :return: New ship position
    """
    ship = Ship()
    for value in values:
        ship.move(value)

    return abs(ship.position[0]) + abs(ship.position[1])

def star_2(values: List[str]):
    """
    Simulate vector ship move

    :param values: Ship instructions
    :return: New ship position
    """
    ship = Ship()
    for value in values:
        ship.vector_move(value)

    return abs(ship.position[0]) + abs(ship.position[1])

def solve(filename: str):
    """
    Get data file, and resolve star problems

    :param filename: Input filename
    :return: Solution of first and second star
    """
    #Read data
    with open(filename) as stream:
        datas = stream.read().splitlines()

    return star_1(datas), star_2(datas)
