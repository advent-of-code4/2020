"""
Resolution of nineteenth day
"""

# from typing import List

# def extract_rules(lines):
#     rules = dict()
#     extract_component = lambda components: [component.split() for component in components]
#     for line in lines:
#         if not line:
#             break
#         index, rule = line.split(":")
#         rules[index] = extract_component(rule.split("|"))
#         if len(rules[index]) == 1 and len(rules[index][0]) == 1 \
#               and not rules[index][0][0].isdigit():
#             rules[index][0] = rules[index][0][0][1]

#     return rules, lines[len(rules):]

# def build_rule(rules, rule_nb) -> List[str]:
#     result = list()
#     for elements in rules[rule_nb]:
#         if type(elements) is not list:
#             return rules[rule_nb]
#         new_rules = [""]

#         for element in elements:
#             results_rule = build_rule(rules, element) #[abb, bba]
#             update_rules = list()
#             for result_rule in results_rule:
#                 for new_rule in new_rules:
#                     update_rules.append(new_rule + result_rule)

#             new_rules = update_rules
#         result += new_rules

#     rules[rule_nb] = result

#     return result

# def star_1(lines: List[str]) -> int:
#     """

#     :param lines:
#     :return:
#     """
#     rules, datas = extract_rules(lines)
#     results = set(build_rule(rules, "0"))

#     return sum([data in results for data in datas])

# def star_2(lines: List[str]):
#     """
#     eaz
#     """
#     pass

def solve(filename: str):
    """
    Get data file, and resolve star problems

    :param filename: Input filename
    :return: Solution of first and second star
    """
    # #Read data
    # with open(filename) as stream:
    #     datas = stream.read().splitlines()

    # return star_1(datas), star_2(datas)
    return filename, filename
