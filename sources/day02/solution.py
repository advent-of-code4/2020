"""
Resolution of second day
"""

from typing import List


def __data_split(data):
    positions, letter, data = data.split(" ")
    first, second = positions.split("-")
    first, second = int(first), int(second)
    letter = letter[0]

    return letter, first, second, data

def star_1(values: List[str]):
    """
    Retrieve number of correct passwords

    :param values: List of passwords
    :return: Number of correct passwords
    """
    result = []
    for value in values:
        letter, minimum, maximum, data = __data_split(value)
        count_letter = data.count(letter)
        result.append(minimum <= count_letter <= maximum)

    return sum(result)

def star_2(values: List[str]):
    """
    Retrieve number of correct passwords

    :param values: List of passwords
    :return: Number of correct passwords
    """
    result = []

    for value in values:
        letter, first, second, data = __data_split(value)
        first, second = first - 1, second - 1
        check = data[first] != data[second]
        check &= letter in (data[first], data[second])
        result.append(check)

    return sum(result)

def solve(filename: str):
    """
    Get data file, and resolve star problems

    :param filename: Input filename
    :return: Solution of first and second star
    """
    #Read data
    with open(filename) as stream:
        datas = list(map(lambda x: x[:-1], stream.readlines()))

    return star_1(datas), star_2(datas)
