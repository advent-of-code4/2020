"""
Resolution of fourth day
"""
import re

from typing import List
from aenum import MultiValueEnum

def check_height(height: str):
    """
    Verify validity of height passport

    :param height: height value
    :return: True if height password is valid
    """
    unity = height[-2:]
    height = height[:-2]
    if unity == "cm":
        return 150 <= int(height) <= 193
    if unity == "in":
        return 59 <= int(height) <= 76
    return False

class Validated(MultiValueEnum):
    """
    Class define map of passport check
    """
    BYR = lambda x: 1920 <= int(x) <= 2002, "byr"
    IYR = lambda x: 2010 <= int(x) <= 2020, "iyr"
    EYR = lambda x: 2020 <= int(x) <= 2030, "eyr"
    HGT = check_height, "hgt"
    HCL = lambda x : re.match('^#[0-9a-f]{6}$', x), "hcl"
    ECL = lambda x: x in {"amb", "blu", "brn", "gry", "grn", "hzl", "oth"}, "ecl"
    PID = lambda x : re.match("^[0-9]{9}$", x) , "pid"
    CID = lambda _: True, "cid"

def __passports(datas: List[str]):
    indices = [i for i, x in enumerate(datas) if x == ""]
    for start, end in zip([0, *indices], [*indices, len(datas)]):
        informations = list()
        for data in datas[start:end+1]:
            if not data: #Remove empty line
                continue
            informations += data.split(" ")
        yield informations

def star_1(values: List[str]):
    """
    Retrieve numbers of valid passport

    :param values: List of passport
    :return: Numbers of valid passport
    """

    valids = list()
    requireds = {"byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"}
    for fields in __passports(values):
        keys = [field.split(":")[0] for field in fields]
        check = all([required in keys for required in requireds])
        valids.append(check)
    return sum(valids)

def star_2(values: List[str]):
    """
    Retrieve numbers of valid passport

    :param values: List of passport
    :return: Numbers of valid passport
    """
    valids = list()
    requireds = {"byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"}
    for fields in __passports(values):
        fields_pair = [field.split(":") for field in fields]
        keys = set(map(lambda x: x[0], fields_pair))

        #Check key presence
        check = all([required in keys for required in requireds])
        if not check:
            valids.append(check)
            continue
        #Check validy of values
        for key, value in fields_pair:
            check = Validated(key).values[0](value)
            if not check:
                break
        valids.append(bool(check))

    return sum(valids)

def solve(filename: str):
    """
    Get data file, and resolve star problems

    :param filename: Input filename
    :return: Solution of first and second star
    """
    #Read data
    with open(filename) as stream:
        datas = list(map(lambda x: x[:-1], stream.readlines()))

    return star_1(datas), star_2(datas)
