"""
Resolution of sixteenth day
"""

from functools import reduce
import re
from typing import List

def __parse(datas: List[str]):
    rules = dict()
    my_ticket = list()
    nearby_tickets = list()

    #Rules
    index = 0
    rule = datas[index]
    while rule:
        search = re.match(r"(.+): (\d+)-(\d+) or (\d+)-(\d+)", rule)
        name = search.group(1)
        nums = list(map(int, search.groups()[1:]))
        rules[name] = nums
        index +=1
        rule = datas[index]

    #Ticket
    index += 2
    my_ticket = list(map(int, datas[index].split(",")))

    #neadbyticket
    index += 3
    datas_size = len(datas)
    while index < datas_size:
        ticket = datas[index]
        nearby_tickets.append(list(map(int, ticket.split(","))))
        index +=1

    return rules, my_ticket, nearby_tickets

def check_rules(rules, value):
    """
    Check if value is valid for rules

    :param rules: Rules verify
    :param values: Value check
    :return: True if value respect any rules else False
    """
    return any([rule[0] <= value <= rule[1] or rule[2] <= value <= rule[3] for rule in rules])

def star_1(values: List[str]) -> int:
    """
    Check all tickets validity

    :param values: List of rules and values
    :return: Sum of not valid tickets
    """
    rules, _, nearby_tickets = __parse(values)

    result = sum([column for ticket in nearby_tickets \
                    for column in ticket if not check_rules(rules.values(), column)])
    return result

def star_2(values: List[str]):
    """
    Check all tickets validity

    :param values: List of rules and values
    :return: Sum of not valid tickets
    """
    rules, my_ticket, nearby_tickets = __parse(values)
    valid_tickets = [ticket for ticket in nearby_tickets \
                        if all([check_rules(rules.values(), column) \
                        for column in ticket])]

    possibles = {}
    for name, bounds in rules.items():
        possibles[name] = [i for i in range(len(rules)) \
                                if all([check_rules([bounds], ticket[i]) \
                                for ticket in valid_tickets])]
    matched = {}

    for name, possibilities in sorted(possibles.items(), key=lambda x: len(x[1])):
        index = [i for i in possibilities if i not in matched]
        assert len(index) == 1
        matched[index[0]] = name

    result = reduce(lambda x,y: x*y, \
        [x for i, x in enumerate(my_ticket) if "departure" in matched[i]])
    return result

def solve(filename: str):
    """
    Get data file, and resolve star problems

    :param filename: Input filename
    :return: Solution of first and second star
    """
    #Read data
    with open(filename) as stream:
        datas = stream.read().splitlines()

    return star_1(datas), star_2(datas)
