"""
Resolution of fourteenth day
"""

from math import log
from typing import List

def __build_mask(mask: str):
    positif = negatif= 0
    floating = list()
    size = len(mask) - 1
    for index, bit in enumerate(mask):
        positif <<= 1
        negatif <<= 1
        if bit == "1":
            positif += 1
        elif bit == "0":
            negatif += 1
        else:
            floating.append(size - index)
    return positif, negatif, floating

def not_op(number, bit_size_m):
    """
    Binary not operator

    :param number: Number
    :param bit_size_m: Max size of bit array
    :return: Result of operation
    """
    bit_size_x = int(log(number, 2)) + 1
    bit_size_x = bit_size_x if bit_size_x > bit_size_m else bit_size_m
    return (1 << bit_size_x) - 1 - number

def generate_mask_combinaison(floating: List[int]):
    """
    Generate combinaison of mask

    :param floating: values with mask
    :return: Mask combinaison
    """
    index = 0
    reset_mask = pow(2,37) - 1
    combinaisons = [0]
    for index in floating:
        new_combi = list()
        reset_mask ^= pow(2, index)
        for combinaison in combinaisons:
            new_combi.append(combinaison | pow(2, index))
        combinaisons += new_combi
    return reset_mask, combinaisons

def star_1(values: List[str]) -> int:
    """
    Apply opperation on all values

    :param values: Input datas
    :return: Sum after all operations
    """
    mem = dict()
    positif = negatif = 0
    for value in values:
        instruction = value.split()
        if instruction[0] == "mask":
            positif, negatif, _ = __build_mask(instruction[-1])
        else:
            address = int(instruction[0][4:-1])
            result = (int(instruction[-1]) | positif)
            mem[address] = result & not_op(negatif, int(log(result, 2)) + 1)
    return sum(mem.values())

def star_2(values: List[str]):
    """
    Apply opperation on all values

    :param values: Input datas
    :return: Sum after all operations
    """
    mem = dict()
    positif = 0
    floating = list()
    for value in values:
        instruction = value.split()
        if instruction[0] == "mask":
            positif, _, floating = __build_mask(instruction[-1])
        else:
            address = int(instruction[0][4:-1]) | positif
            reset_mask, masks_flot = generate_mask_combinaison(floating)
            new_adds = list(map(lambda mask_float: (address & reset_mask) | mask_float, masks_flot))
            for new_add in new_adds:
                mem[new_add] = int(instruction[-1])

    return sum(mem.values())

def solve(filename: str):
    """
    Get data file, and resolve star problems

    :param filename: Input filename
    :return: Solution of first and second star
    """
    #Read data
    with open(filename) as stream:
        datas = stream.read().splitlines()

    return star_1(datas), star_2(datas)
