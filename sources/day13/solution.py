"""
Resolution of thirteenth day
"""

from typing import List

def star_1(values: List[str]) -> int:
    """
    Calculate the number of minutes you'll need to wait for that bus

    :param values: List of bus
    :return: Time need to wait bus
    """
    timestamp = int(values[0])
    bus_list = [int(value) for value in values[1].split(",") if value != "x"]

    pos, delay = 0, bus_list[0] - timestamp % bus_list[0]
    for index, bus in enumerate(bus_list[1:]):
        calc = bus - timestamp % bus
        if calc > delay:
            continue
        pos, delay = index + 1, calc

    return bus_list[pos] * delay

# def star_2(values: List[str]):
#     """
#     eaz
#     """
#     pass

def solve(filename: str):
    """
    Get data file, and resolve star problems

    :param filename: Input filename
    :return: Solution of first and second star
    """
    #Read data
    with open(filename) as stream:
        datas = stream.read().splitlines()

    return star_1(datas), None
