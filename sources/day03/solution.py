"""
Resolution of third day
"""

from typing import List


def make_toboggan(right, down, datas):
    """
    Retrieve numbers of tree seen

    :param right: Right shifting number
    :param down: Down shifting number
    :param datas: Local geology data
    :return: Numbers of tree seen
    """
    tree = 0
    position = 0
    size = len(datas[0])
    for index in range(0, len(datas)-down, down):
        position = (position+right)%size
        tree += 1 if datas[index+down][position] =="#" else 0

    return tree

def star_1(values: List[str]):
    """
    Retrieve numbers of tree seen

    :param values: Local geology data
    :return: Number of tree seen
    """
    return make_toboggan(3, 1, values)

def star_2(values: List[str]):
    """
    Retrieve numbers of tree seen

    :param values: Local geology data
    :return: Numbers of tree seen
    """

    strats = [(1,1), (3,1), (5, 1), (7, 1), (1, 2)]
    result = 1
    for right, down in strats:
        result *= make_toboggan(right, down, values)

    return result

def solve(filename: str):
    """
    Get data file, and resolve star problems

    :param filename: Input filename
    :return: Solution of first and second star
    """
    #Read data
    with open(filename) as stream:
        datas = list(map(lambda x: x[:-1], stream.readlines()))

    return star_1(datas), star_2(datas)
