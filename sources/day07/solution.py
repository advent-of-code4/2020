"""
Resolution of seventh day
"""

from typing import List, Tuple, Any
from pydantic import BaseModel

class Node(BaseModel):
    """
    Class define Bag node
    """
    name: str
    fathers: List[Tuple[int, Any]] = list()
    childrens: List[Tuple[int, Any]]  = list()
    include: bool = False
    child_nb = 1

    def add_children(self, number: int, children):
        """
        Add sub bags on this bag

        :param number: Number of bags
        :param children: Bag add
        """
        self.childrens.append((number, children))
        children.fathers.append((1, self))

    def reverse_russian_doll(self):
        """
        Calculate nombre of colors bags can contain this bag

        :return: Number of colors bags
        """
        if self.include:
            return 0
        self.include = True
        total = 1
        for _, father in self.fathers:
            total += father.reverse_russian_doll()
        return total

    def russian_doll(self):
        """
        Calculate numbre of individual bags one shiny gold bag

        :return: Number of individual bags
        """
        if self.include:
            return self.child_nb
        self.include = True
        total = 1
        for number, child in self.childrens:
            total += number * child.russian_doll()
        self.child_nb = total
        return total

    def __hash__(self):
        return hash(self.name)

    def __eq__(self, name):
        return self.name == name

def __parse_rule(rule: str):
    father, childrens = rule[:-1].split(" contain ")
    father = father[:-1]
    if childrens == "no other bags":
        return father, list()

    childrens = childrens.split(", ")
    update_childrens = list()
    for children in childrens:
        number = int(children[0])
        name = children[2:-1] if number > 1 else children[2:]
        update_childrens.append((number, name))

    return father, update_childrens

def __build_tree(values: List[str]):
    nodes = dict()
    for rule in values:
        father, childrens = __parse_rule(rule)
        #Create node if not exist
        if not father in nodes:
            father = Node(**{"name": father})
            nodes[father] = father
        else:
            father = nodes[father]

        for number, children_name in childrens:
            if not children_name in nodes:
                children = Node(**{"name": children_name})
                nodes[children] = children
            else:
                children = nodes[children_name]
            father.add_children(number, children)
    return nodes

def star_1(values: List[str]) -> int:
    """
    Calculate nombre of colors bags can contain one shiny gold bag

    :param values: Rules of bags
    :return: Number of colors bags
    """
    nodes = __build_tree(values)
    return nodes["shiny gold bag"].reverse_russian_doll() - 1

def star_2(values: List[str]):
    """
    Calculate numbre of individual bags one shiny gold bag

    :param values: Rules of bags
    :return: Number of individual bags
    """
    nodes = __build_tree(values)
    return nodes["shiny gold bag"].russian_doll() - 1

def solve(filename: str):
    """
    Get data file, and resolve star problems

    :param filename: Input filename
    :return: Solution of first and second star
    """
    #Read data
    with open(filename) as stream:
        datas = list(map(lambda x: x[:-1], stream.readlines()))

    return star_1(datas), star_2(datas)
