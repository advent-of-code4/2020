"""
Resolution of first day
"""

from typing import List


def star_1(values: List[int], number: int = 2020):
    """
    Retrieve complementaries numbers who compose a number

    :param values: List of numbers
    :param number: Number used for search
    :return: Product of complementaries numbers
    """
    complementaries = set()
    for value in values:
        if value in complementaries:
            return value * (number - value)
        complementaries.add(number - value)
    return None

def star_2(values: List[int]):
    """
    Retrieve complementaries numbers who compose a number

    :param values: List of numbers
    :return: Product of complementaries numbers
    """

    for value in values:
        number = 2020 - value
        solution = star_1(values, number)

        if solution:
            return value * solution
    return None


def solve(filename: str):
    """
    Get data file, and resolve star problems

    :param filename: Input filename
    :return: Solution of first and second star
    """
    #Read data
    with open(filename) as stream:
        datas = list(map(lambda x: int(x[:-1]), stream.readlines()))

    return star_1(datas), star_2(datas)
