"""
Resolution of sixth day
"""

from typing import List

def __group_form(datas: List[str]):
    indices = [i for i, x in enumerate(datas) if x == ""]
    for start, end in zip([0, *indices], [*indices, len(datas)]):
        answer = list()
        for data in datas[start:end+1]:
            if not data: #Remove empty line
                continue
            answer.append(data)
        yield answer

def star_1(values: List[str]):
    """
    Retrieve number of form which anyone answered "yes"

    :param values: List of group answers
    :return: Number of form
    """
    answers = __group_form(values)
    total = 0
    for answer in answers:
        tmp = set()
        for response in answer:
            tmp = {*tmp, *set(response)}
        total += len(tmp)

    return total

def star_2(values: List[str]):
    """
    Retrieve number of form which everyone answered "yes"

    :param values: List of group answers
    :return: Number of form
    """
    answers = __group_form(values)
    total = 0
    for answer in answers:
        result = set(answer[0])
        for response  in answer[1:]:
            result = result.intersection(set(response))
        total += len(result)
    return total

def solve(filename: str):
    """
    Get data file, and resolve star problems

    :param filename: Input filename
    :return: Solution of first and second star
    """
    #Read data
    with open(filename) as stream:
        datas = list(map(lambda x: x[:-1], stream.readlines()))

    return star_1(datas), star_2(datas)
