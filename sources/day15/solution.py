"""
Resolution of fifteenth day
"""

from numba import njit
from numba.typed import List

@njit
def str_to_int(string) -> int:
    """
    Convert string to int

    :param string: Value
    :return: Int value
    """
    final_index, result = len(string) - 1, 0
    for index,value in enumerate(string):
        result += (ord(value) - 48) * (10 ** (final_index - index))
    return result

@njit
def star_1(values, nb_turn = 2020) -> int:
    """
    Calculate all iteration of values

    :param values: List of value start
    :param nb_turn: Number of iterations
    :return: Result
    """
    spoken = dict()
    start = values[0].split(",")
    for index, value in enumerate(start[:-1]):
        spoken[str_to_int(value)] = index + 1

    current = str_to_int(start[-1])
    index_2 = len(start) - 1
    for _ in range(nb_turn - len(start)):
        index_2 += 1
        if current in spoken:
            last = spoken[current]
            spoken[current] = index_2
            current = index_2 - last
        else:
            spoken[current] = index_2
            current = 0
    return current

def star_2(values):
    """
    Calculate all iteration of values

    :param values: List of value start
    :param nb_turn: Number of iterations
    :return: Result
    """
    return star_1(values, 30000000)


def solve(filename: str):
    """
    Get data file, and resolve star problems

    :param filename: Input filename
    :return: Solution of first and second star
    """
    #Read data
    with open(filename) as stream:
        datas = stream.read().splitlines()
    typed_a = List()
    for data in datas:
        typed_a.append(data)

    return star_1(typed_a), star_2(typed_a)
