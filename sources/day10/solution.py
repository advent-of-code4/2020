"""
Resolution of tenth day
"""

from typing import List

def __order(values: List[str]):
    datas = list(map(int, values))
    datas.sort()
    datas.insert(0, 0)
    datas.append(datas[-1] + 3)

    return datas

def star_1(values: List[str]) -> int:
    """
    Calculate the number of adapter necessary for  charging the device

    :param values: List of adapters
    :return: The number of 1-jolt differences multiplied by the number of 3-jolt
    """
    datas = __order(values)
    datas_size = len(datas)

    dif_1 = dif_3 = 0
    for index in range(0, datas_size - 1):
        dif = datas[index + 1] - datas[index]
        if dif == 1:
            dif_1 +=1
        elif dif == 3:
            dif_3 += 1

    return dif_1 * dif_3

def star_2(values: List[str]):
    """
    Calculate the number of distinct ways you can arrange the adapters
    to connect the charging outlet

    :param values: list of adaptaters
    :return: Number of combinaison
    """
    datas = __order(values)
    datas_size = len(datas)
    diffs = [datas[index + 1] - datas[index] for index in range(0, datas_size - 1)]

    nb_combi = 1
    chunck = list()
    for diff in diffs:
        if diff == 3 and chunck:
            chunck_size = len(chunck) - 1
            nb_combi *= pow(2, chunck_size) if chunck_size <= 2 else pow(2, chunck_size) - 1
            chunck = list()
        elif diff <3:
            chunck.append(diff)

    return nb_combi

def solve(filename: str):
    """
    Get data file, and resolve star problems

    :param filename: Input filename
    :return: Solution of first and second star
    """
    #Read data
    with open(filename) as stream:
        datas = stream.readlines()

    return star_1(datas), star_2(datas)
