"""
Resolution of seventeenth day
"""

from typing import List

import numpy as np
from scipy.signal import convolve

def parse(datas: List[str]):
    """
    Cast List string grid to numpy grid

    :param datas: Starting grid
    :return: New grid
    """
    converter = str.maketrans('.#', '01')
    return np.array([[int(x) for x in list(data.translate(converter))] \
                        for data in datas], dtype=np.uint8)

def cycle(init, dim):
    """
    Run 6 steps of grid

    :param init: Starting grid
    :param dim: Dimention of grid evolution
    :return: Status of grid after steps apply
    """
    grid = init.reshape([1] * (dim - len(init.shape)) + list(init.shape))
    chunck = np.ones([3] * dim, dtype=np.uint8)
    for _ in range(6):
        number = convolve(grid, chunck)
        grid = np.pad(grid, ((1,1),), mode='constant') & (number == 4) | (number == 3)
    return np.sum(grid)

def star_1(values: List[str]) -> int:
    """
    Run 6 steps of grid

    :param values: Starting grid
    :return: Status of grid after steps apply
    """
    grid = parse(values)
    return cycle(grid, 3)

def star_2(values: List[str]):
    """
    Run 6 steps of grid

    :param values: Starting grid
    :return: Status of grid after steps apply
    """
    grid = parse(values)
    return cycle(grid, 4)

def solve(filename: str):
    """
    Get data file, and resolve star problems

    :param filename: Input filename
    :return: Solution of first and second star
    """
    #Read data
    with open(filename) as stream:
        datas = stream.read().splitlines()

    return star_1(datas), star_2(datas)
