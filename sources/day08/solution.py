"""
Resolution of heighth day
"""

from typing import List

from aenum import MultiValueEnum
from pydantic import BaseModel

class EnumInstruction(MultiValueEnum):
    """
    Enum define instruction mapping
    """
    JUMP = lambda acc, index, val: (acc, val + index), "jmp"
    NOP = lambda acc, index, _: (acc, index + 1), "nop"
    ACC = lambda acc, index, val: (val + acc, index + 1), "acc"

class Instruction(BaseModel):
    """
    Class define Instruction
    """
    instruction: EnumInstruction
    value: int
    active: bool = False

    def run(self, accumulator, index, default_active = True):
        """
        Retrieve accumulator and index after execute instruction

        :param accumulator: Execution resulte
        :param index: Index execution
        :return: Result of instruction execution
        """
        if self.active:
            return accumulator, None
        self.active = default_active
        return self.instruction.value(accumulator, index, self.value)

    def get_instruction(self):
        """
        Retrieve intruction

        :return: Instruction
        """
        return self.instruction

    def update_instruction(self, instruction):
        """
        Update instruction

        :param instruction: New instruction
        """
        self.instruction = instruction

def __parse_data(datas: List[str]) -> List[Instruction]:
    instructions = list()
    for data in datas:
        instruction, val = data.split()
        ins = Instruction(**{"instruction": instruction, "value": int(val)})
        instructions.append(ins)
    return instructions

def star_1(values: List[str]) -> int:
    """
    Execute instructions and return result

    :param values: List of instructions
    :return: Final value of execution
    """

    instructions = __parse_data(values)
    accumulator = index = 0
    while True:
        accumulator, index = instructions[index].run(accumulator, index)
        if index is None:
            return accumulator

def star_2(values: List[str]):
    """
    Execute instructions and return result

    :param values: List of instructions
    :return: Final value of execution
    """
    instructions = __parse_data(values)
    size_ins = len(instructions)
    for instruction in instructions:
        ins = instruction.get_instruction()
        if ins == EnumInstruction.ACC:
            continue

        upd = EnumInstruction.JUMP if ins == EnumInstruction.NOP else EnumInstruction.NOP
        instruction.update_instruction(upd)
        accumulator = index = 0
        save_index = set()
        while index not in save_index and index < size_ins:
            save_index.add(index)
            accumulator, index = instructions[index].run(accumulator, index, False)
        if index == size_ins:
            return accumulator
        instruction.update_instruction(ins)
    return None

def solve(filename: str):
    """
    Get data file, and resolve star problems

    :param filename: Input filename
    :return: Solution of first and second star
    """
    #Read data
    with open(filename) as stream:
        datas = stream.readlines()

    return star_1(datas), star_2(datas)
