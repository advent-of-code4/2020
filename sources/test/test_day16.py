"""
Test for day16 of advent of code
"""

import pytest
import day16.solution as day16


@pytest.mark.parametrize("datas, solution, algo",[
    ("day16/test_input.txt",
        (203, 143), day16.solve),
    ("day16/input.txt",
        (21956, 3709435214239), day16.solve)
])
def test(datas, solution, algo):
    """
    Defined test function executed
    """
    assert algo(datas) == solution
