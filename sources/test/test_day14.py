"""
Test for day14 of advent of code
"""

import pytest
import day14.solution as day14


@pytest.mark.parametrize("datas, solution, algo",[
    ("day14/test_input_2.txt",
        (51, 208), day14.solve),
    ("day14/input.txt",
        (5875750429995, 5272149590143), day14.solve)
])
def test(datas, solution, algo):
    """
    Defined test function executed
    """
    assert algo(datas) == solution
