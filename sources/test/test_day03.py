"""
Test for day03 of advent of code
"""

import pytest
import day03.solution as day03

test_datas = ["..##.......",
            "#...#...#..",
            ".#....#..#.",
            "..#.#...#.#",
            ".#...##..#.",
            "..#.##.....",
            ".#.#.#....#",
            ".#........#",
            "#.##...#...",
            "#...##....#",
            ".#..#...#.#"]
@pytest.mark.parametrize("datas, solution, algo",[
    (test_datas,
        7, day03.star_1),
    (test_datas ,
        336, day03.star_2),
    ("day03/input.txt",
        (205, 3952146825), day03.solve)
])
def test(datas, solution, algo):
    """
    Defined test function executed
    """
    assert algo(datas) == solution
