"""
Test for day13 of advent of code
"""

import pytest
import day13.solution as day13


@pytest.mark.parametrize("datas, solution, algo",[
    ("day13/test_input.txt",
        (295, None), day13.solve),
    # ("day13/test_input2.txt",
    #     (295, 1068788), day13.solve)

    # ("day13/test2_input.txt",
    #     (220, 19208), day13.solve),
    # ("day13/input.txt",
    #     (1690, 5289227976704), day13.solve)
])
def test(datas, solution, algo):
    """
    Defined test function executed
    """
    assert algo(datas) == solution
