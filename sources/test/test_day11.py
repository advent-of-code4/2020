"""
Test for day11 of advent of code
"""

import pytest
import day11.solution as day11


@pytest.mark.parametrize("datas, solution, algo",[
    ("day11/test_input.txt",
        (37, 26), day11.solve),
    ("day11/input.txt",
        (2489, 2180), day11.solve)
])
def test(datas, solution, algo):
    """
    Defined test function executed
    """
    assert algo(datas) == solution
