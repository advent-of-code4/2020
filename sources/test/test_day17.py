"""
Test for day17 of advent of code
"""

import pytest
import day17.solution as day17


@pytest.mark.parametrize("datas, solution, algo",[
    ("day17/input.txt",
        (213, 1624), day17.solve)
])
def test(datas, solution, algo):
    """
    Defined test function executed
    """
    assert algo(datas) == solution
