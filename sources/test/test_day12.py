"""
Test for day12 of advent of code
"""

import pytest
import day12.solution as day12


@pytest.mark.parametrize("datas, solution, algo",[
    ("day12/test_input.txt",
        (25, 286), day12.solve),
    ("day12/input.txt",
        (845, 27016), day12.solve)
])
def test(datas, solution, algo):
    """
    Defined test function executed
    """
    assert algo(datas) == solution
