"""
Test for day15 of advent of code
"""

import pytest
import day15.solution as day15


@pytest.mark.parametrize("datas, solution, algo",[
    ("day15/input.txt",
        (1259, 689), day15.solve)
])
def test(datas, solution, algo):
    """
    Defined test function executed
    """
    assert algo(datas) == solution
