"""
Test for day01 of advent of code
"""

import pytest
import day01.solution as day01

@pytest.mark.parametrize("datas, solution, algo",[
    ([1721, 979, 366, 299, 675, 1456] ,
        514579, day01.star_1),
    ([1721, 979, 366, 299, 675, 1456] ,
        241861950, day01.star_2),
    ([1721, 979] ,
        None, day01.star_2),
    ("day01/input.txt",
        (703131, 101416770), day01.solve)
])
def test(datas, solution, algo):
    """
    Defined test function executed
    """
    assert algo(datas) == solution
