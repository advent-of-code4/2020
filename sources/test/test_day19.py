"""
Test for day19 of advent of code
"""

import pytest
import day19.solution as day19


@pytest.mark.parametrize("datas, solution, algo",[
    ("day19/test_input.txt",
        ("day19/test_input.txt", "day19/test_input.txt"), day19.solve),
    # ("day19/input.txt",
    #     (147, None), day19.solve)

])
def test(datas, solution, algo):
    """
    Defined test function executed
    """
    assert algo(datas) == solution
