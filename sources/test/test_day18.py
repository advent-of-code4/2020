"""
Test for day18 of advent of code
"""

import pytest
import day18.solution as day18


@pytest.mark.parametrize("datas, solution, algo",[
    ("day18/input.txt",
        (8929569623593, 231235959382961), day18.solve)
])
def test(datas, solution, algo):
    """
    Defined test function executed
    """
    assert algo(datas) == solution
