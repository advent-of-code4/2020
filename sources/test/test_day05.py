"""
Test for day05 of advent of code
"""

import pytest
import day05.solution as day05

@pytest.mark.parametrize("datas, solution, algo",[
    (["FBFBBFFRLR"],
        357, day05.star_1),
    (["FBFBBFFRLR"],
        357, day05.star_1),
    (["FBFBBFFRLR"] ,
        None, day05.star_2),
    ("day05/input.txt",
        (955, 569), day05.solve)
])
def test(datas, solution, algo):
    """
    Defined test function executed
    """
    assert algo(datas) == solution
