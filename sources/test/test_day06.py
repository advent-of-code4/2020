"""
Test for day06 of advent of code
"""

import pytest
import day06.solution as day06

test_form = ["abc",
            "",
            "a",
            "b",
            "c",
            "",
            "ab",
            "ac",
            "",
            "a",
            "a",
            "a",
            "a",
            "",
            "b"]

@pytest.mark.parametrize("datas, solution, algo",[
    (test_form,
        11, day06.star_1),
    (test_form,
        6, day06.star_2),
    ("day06/input.txt",
        (7110, 3628), day06.solve)
])
def test(datas, solution, algo):
    """
    Defined test function executed
    """
    assert algo(datas) == solution
