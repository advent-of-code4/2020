"""
Test for day10 of advent of code
"""

import pytest
import day10.solution as day10


@pytest.mark.parametrize("datas, solution, algo",[
    ("day10/test_input.txt",
        (35, 8), day10.solve),
    ("day10/test2_input.txt",
        (220, 19208), day10.solve),
    ("day10/input.txt",
        (1690, 5289227976704), day10.solve)
])
def test(datas, solution, algo):
    """
    Defined test function executed
    """
    assert algo(datas) == solution
