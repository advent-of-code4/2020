"""
Test for day02 of advent of code
"""

import pytest
import day02.solution as day02

@pytest.mark.parametrize("datas, solution, algo",[
    (["1-3 a: abcde", "1-3 b: cdefg", "2-9 c: ccccccccc"] ,
        2, day02.star_1),
    (["1-3 a: abcde", "1-3 b: cdefg", "2-9 c: ccccccccc"] ,
        1, day02.star_2),
    ("day02/input.txt",
        (460, 251), day02.solve)
])
def test(datas, solution, algo):
    """
    Defined test function executed
    """
    assert algo(datas) == solution
