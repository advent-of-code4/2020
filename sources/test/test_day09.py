"""
Test for day09 of advent of code
"""

import pytest
import day09.solution as day09

test_data  =["35",
            "20",
            "15",
            "25",
            "47",
            "40",
            "62",
            "55",
            "65",
            "95",
            "102",
            "117",
            "150",
            "182",
            "127",
            "219",
            "299",
            "277",
            "309",
            "576"]

test_data_2  =["1",
            "2",
            "3",
            "4",
            "5",
            "6"]

@pytest.mark.parametrize("datas, solution, algo",[
    (test_data,
        127, day09.star_1),
    (test_data_2,
        None, day09.star_1),

    (test_data,
        62, day09.star_2),
    (test_data_2,
        None, day09.star_2),
    ("day09/input.txt",
        (466456641, 55732936), day09.solve)
])
def test(datas, solution, algo):
    """
    Defined test function executed
    """
    assert algo(datas) == solution
