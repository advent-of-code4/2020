"""
Test for day08 of advent of code
"""

import pytest
import day08.solution as day08

test_data  =["nop +0",
                "acc +1",
                "jmp +4",
                "acc +3",
                "jmp -3",
                "acc -99",
                "acc +1",
                "jmp -4",
                "acc +6"]

test_data_2  =["nop +0",
                "acc +1",
                "acc +4",
                "acc +3",
                "jmp -3",
                "acc -99",
                "acc +1",
                "jmp -4",
                "acc +6"]

@pytest.mark.parametrize("datas, solution, algo",[
    (test_data,
        5, day08.star_1),
    (test_data,
        8, day08.star_2),
    (test_data_2,
        None, day08.star_2),
    ("day08/input.txt",
        (1949, 2092), day08.solve)
])
def test(datas, solution, algo):
    """
    Defined test function executed
    """
    assert algo(datas) == solution
