"""
Run all day of advent of code
"""

import os

def load_days(path="."):
    """
    Found and load sub module

    :param path: Root we look at
    :return: List of advent of code day
    """

    days_exist = list()
    for name in os.listdir(path):
        check = os.path.isfile(os.path.join(path, name))
        check |= name[:3] != "day"
        if check:
            continue
        days_exist.append(name)

    days_exist.sort()
    return list(map(lambda module: __import__(module + ".solution"), days_exist))

if __name__ == "__main__":
    days = load_days()
    for day in days:
        day_name = day.__name__
        star1, star2 = day.solution.solve("{0}/input.txt".format(day_name))
        TEXT = "Advent of code {0}: {1} / {2}".format(day_name, star1, star2)
        print(TEXT)
