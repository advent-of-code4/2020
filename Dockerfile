FROM python:3.7-alpine

#Install dependances
RUN apk add build-base bash openblas-dev lapack llvm-dev
COPY requirement.txt /        
RUN pip install -r /requirement.txt

#Code
COPY sources /code
WORKDIR /code

