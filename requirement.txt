#Test Lib
pytest==6.1.2
pytest-cov==2.10.1

# 
pylint==2.6.0

#Use for multi mapping enum
aenum==2.2.4
pydantic==1.7.3
numpy==1.19.5
scipy==1.6.0
llvmlite==0.35.0
numba==0.50.1